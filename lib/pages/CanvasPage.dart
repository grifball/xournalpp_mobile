import 'dart:math' as math;
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
//import 'package:photo_view/photo_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:vector_math/vector_math_64.dart' show Vector4;
import 'package:vector_math/vector_math_64.dart' show Vector3;
import 'package:vector_math/vector_math_64.dart' show Quad;
import 'package:xournalpp/generated/l10n.dart';
import 'package:xournalpp/src/XppFile.dart';
import 'package:xournalpp/src/XppBackground.dart';
import 'package:xournalpp/src/XppPage.dart';
import 'package:xournalpp/src/globals.dart';
import 'package:xournalpp/widgets/EditingToolbar.dart';
import 'package:xournalpp/widgets/MainDrawer.dart';
import 'package:xournalpp/widgets/PointerListener.dart';
import 'package:xournalpp/widgets/ToolBoxBottomSheet.dart';
import 'package:xournalpp/widgets/XppPageStack.dart';
import 'package:xournalpp/widgets/XppPagesListView.dart';
import 'package:xournalpp/widgets/ZoomableWidget.dart';

class CanvasPage extends StatefulWidget {
  CanvasPage({Key? key, this.file, this.filePath}) : super(key: key);

  @required
  final XppFile? file;
  final String? filePath;

  @override
  _CanvasPageState createState() => _CanvasPageState();
}

class _CanvasPageState extends State<CanvasPage> with TickerProviderStateMixin {
  XppFile? _file;

  int currentPage = 0;

  Color toolColor = Colors.blueGrey;
  double toolWidth = 5;

  TransformationController _zoomController = TransformationController();

  Map<PointerDeviceKind?, EditingTool> _toolData = {};
  PointerDeviceKind? _currentDevice = PointerDeviceKind.touch;

  /// used fro parent-child communication
  Map<int, GlobalKey<XppPageStackState>> _pageStackKeys = {};
  Map<int, GlobalKey<PointerListenerState>> pointerListenerKeys = {};
  final GlobalKey<EditingToolBarState> _editingToolbarKey = GlobalKey();
  //final GlobalKey<PointerListenerState> _pointerListenerKey = GlobalKey();
  final GlobalKey<ZoomableWidgetState> _zoomableKey = GlobalKey();
  final GlobalKey<XppPagesListViewState> pageListViewKey = GlobalKey();
  //final GlobalKey<SingleChildScrollViewState> singleChildScrollerState = GlobalKey();

  double pageScale = 1;

  bool savingFile = false;

  Animation<Matrix4>? _animationReset;
  late AnimationController _controllerReset;

  @override
  void initState() {
    _setMetadata();
    super.initState();
    _controllerReset = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
    );
    loadToolSettings();
  }
	static Rect axisAlignedBoundingBox(Quad quad) {
		final double minX = math.min(
			quad.point0.x,
			math.min(
				quad.point1.x,
				math.min(
					quad.point2.x,
					quad.point3.x,
				),
			),
		);
		final double minY = math.min(
			quad.point0.y,
			math.min(
				quad.point1.y,
				math.min(
					quad.point2.y,
					quad.point3.y,
				),
			),
		);
		final double maxX = math.max(
			quad.point0.x,
			math.max(
				quad.point1.x,
				math.max(
					quad.point2.x,
					quad.point3.x,
				),
			),
		);
		final double maxY = math.max(
			quad.point0.y,
			math.max(
				quad.point1.y,
				math.max(
					quad.point2.y,
					quad.point3.y,
				),
			),
		);
		return Rect.fromLTRB(minX, minY, maxX, maxY);
//		return Quad.points(
//			Vector3(minX, minY, 0),
//			Vector3(maxX, minY, 0),
//			Vector3(maxX, maxY, 0),
//			Vector3(minX, maxY, 0),
//		);
	}
  
  int _numberRows = 10;
  int _numberColumns = 20;
  double _cellHeight = XppPageSize.a4.width!;//1000;
  double _cellWidth = XppPageSize.a4.height!;//1000;
  Quad? _cachedViewport;
  int _firstVisibleRow = 0;
  int _firstVisibleColumn = 0;
  int _lastVisibleRow = 0;
  int _lastVisibleColumn = 0;
  bool _isCellVisible(int row, int column, Quad viewport) {
    if (viewport != _cachedViewport) {
      final Rect aabb = axisAlignedBoundingBox(viewport);
      _cachedViewport = viewport;
      _firstVisibleRow = (aabb.top / _cellHeight).floor();
      _firstVisibleColumn = (aabb.left / _cellWidth).floor();
      _lastVisibleRow = (aabb.bottom / _cellHeight).floor();
      _lastVisibleColumn = (aabb.right / _cellWidth).floor();
    }
    return row >= _firstVisibleRow && row <= _lastVisibleRow && column >= _firstVisibleColumn && column <= _lastVisibleColumn;
  }
	void _leave(BuildContext context) {
			showDialog(
					context: context,
					builder: (BuildContext ctx) {
						return AlertDialog(
							title: const Text('Please Confirm'),
							content: const Text('Are you sure to leave?'),
							actions: [
								// The "Yes" button
								TextButton(
										onPressed: () {
											// Close the dialog (and the page)
											Navigator.of(context).pop();
											Navigator.of(context).pop();
										},
										child: const Text('Yes')),
								TextButton(
										onPressed: () {
											// Close the dialog
											Navigator.of(context).pop();
										},
										child: const Text('No'))
							],
						);
					});
		}

  @override
  Widget build(BuildContext context) {
    final zoomEnabled = _toolData[_currentDevice] == null ||
        _toolData[_currentDevice] == EditingTool.MOVE;
    _numberRows = _file!.pages!.length;
    _numberColumns = 1;
    for (int pageIndex=0; pageIndex < _file!.pages!.length; pageIndex++) {
      if (!_pageStackKeys.keys.contains(pageIndex)) _pageStackKeys[pageIndex] = GlobalKey();
      if (!pointerListenerKeys.keys.contains(pageIndex)) pointerListenerKeys[pageIndex] = GlobalKey();
    }
    return WillPopScope(
      onWillPop: () async {
				_leave(context);
        return false;
      },
      child: Scaffold(
        //resizeToAvoidBottomPadding: false,
        //resizeToAvoidBottomInset: false,
        drawer: MainDrawer(),
        body: Stack(
          children: [
          LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return ZoomableWidget(
                //scaleEnabled: false,
                key: _zoomableKey,
                controller: _zoomController,
                onInteractionStart: _onInteractionStart,
                onInteractionUpdate: (details) {
                  //print(details);
                  setState(() => pageScale = _zoomController.value.entry(0, 0));
                },

                builder: (BuildContext context, Quad viewport) {
                  return Column(
                    children: <Widget>[
                      for (int row=0; row < _numberRows; row++)
                        Row(
                          children: <Widget>[
                            for (int column = 0; column < _numberColumns; column++)
                              _isCellVisible(row, column, viewport)
                                ? Container(
                                  height: _cellHeight,
                                  width: _cellWidth,
                                  color: Colors.amber[100],
                                  child: Center(
                                    child: Card(
                                      elevation: 12,
                                      color: Colors.white,
                                      child: AspectRatio(
                                        aspectRatio: _file!.pages![row].pageSize!.ratio,
                                        child: PointerListener(
                                          key: pointerListenerKeys[row],
                                          translationMatrix: _zoomController.value,
                                          toolData: _toolData,
                                          strokeWidth: toolWidth,
                                          color: toolColor,
                                          onDeviceChange: (
                                              {int? device, PointerDeviceKind? kind}) {
                                            setDefaultDeviceIfNotSet(kind: kind);
                                            _currentDevice = kind;
                                            _editingToolbarKey.currentState!.setState(() {
                                              _editingToolbarKey.currentState!.currentDevice =
                                                  kind;
                                              _setZoomableState();
                                            });
                                          },
                                          removeLastContent: () {
                                            _file!.pages![row].layers![0].content!.removeLast();
                                          },
                                          filterEraser: ({Offset? coordinates, double? radius}) {
                                            // if we would execute the removal instantly, we would destroy the order of the strokes
                                            List<Function> removalFunctions = [];
                                            _file!.pages![row].layers![0].content!
                                                .forEach((stroke) {
                                              final delta = stroke!.eraseWhere(
                                                  coordinates: coordinates, radius: radius);
                                              if (!delta.affected) return;
                    
                                              removalFunctions.add(() {
                                                final int index = _file!
                                                    .pages![row].layers![0].content!
                                                    .indexOf(stroke);
                                                _file!.pages![row].layers![0].content!
                                                    .removeAt(index);
                                                _file!.pages![row].layers![0].content!
                                                    .insertAll(index, delta.newContent);
                                              });
                                            });
                                            if (removalFunctions.isNotEmpty) {
                                              removalFunctions.forEach((element) {
                                                element();
                                              });
                                              setState(() {});
                                            }
                                          },
                                          onNewContent: (newContent) {
                                            /// TODO: manage layers
                                            _file!.pages![row].layers![0].content =
                                                new List.from(_file!
                                                    .pages![row].layers![0].content!)
                                                  ..add(newContent);
                    
                                            _pageStackKeys[row]!.currentState!.setPageData(_file!.pages![row]);
                                          },
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(4),
                                            child: XppPageStack(
                                              /// to communicate from [PointerListener] to [XppPageStack]
                                              key: _pageStackKeys[row],
                                              page: _file!.pages![row],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ),
                                )
                                : Container(
                                  height: _cellHeight,
                                  width: _cellWidth,
                                ),
                          ]
                        ),
                    ],
                  );
                },
              );
            },
          ),
          ]
        ),
        appBar: AppBar(
          title: Tooltip(
            message: S.of(context).doubleTapToChange,
            child: GestureDetector(
                onDoubleTap: _showTitleDialog,
                child: Text(widget.file?.title ?? S.of(context).newDocument)),
          ),
          actions: [
            savingFile
                ? Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(
                            Theme.of(context).colorScheme.onPrimary),
                      ),
                    ),
                  )
                : IconButton(
                    icon: Icon(Icons.save),
                    onPressed: saveFile,
                    tooltip: S.of(context).save,
                  ),
            PopupMenuButton<String>(
              onSelected: (item) async {
                if (item == S.of(context).saveAs) saveFile(export: true);
                if (item == S.of(context).sharePage) shareScreenshot();
              },
              itemBuilder: (BuildContext context) {
                return {
                  S.of(context).saveAs,
                  if (!kIsWeb) S.of(context).sharePage
                }.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ],
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(64),
              child: EditingToolBar(
                  key: _editingToolbarKey,
                  deviceMap: _toolData,
                  getColor: getColor,
                  getWidth: getWidth,
                  onWidthChange: (newWidth) {
                    rememberToolSettings();
                    setState(() {
                      toolWidth = newWidth *
                          2; // average pressure is 0.5, so multiplying by 2

                    });
                  },
                  onColorChange: (newColor) {
                    rememberToolSettings();
                    toolColor = newColor;
                    setState(() {
                      toolColor = newColor;
                    });
                  },
                  onNewDeviceMap: (newDeviceMap) => setState(
                        () {
                          _toolData = newDeviceMap!;
                          _setZoomableState();
                        },
                      ))),
        ),
  //      bottomNavigationBar:
  //      BottomAppBar(
  //        shape: kIsWeb ? null : CircularNotchedRectangle(),
  //        child: Container(
  //            width: 300,
  //            color: Theme.of(context).colorScheme.surface,
  //            constraints: BoxConstraints(maxHeight: 100),
  //            child: ListView(
  //              scrollDirection: Axis.horizontal,
  //              children: [
  //                Container(
  //                  //width: 300,
  //                  child:
  //                  XppPagesListView(
  //                    key: pageListViewKey,
  //                    pages: _file!.pages,
  //                    //pages: [XppPage.empty()],
  //                    onPageChange: (newPage) {
  //                      setState(() => currentPage = newPage);
  ////                      _pageStackKey.currentState!
  ////                          .setPageData(_file!.pages![currentPage]);
  //                    },
  //                    onPageDelete: (deletedIndex) => setState(() {
  //                          _file!.pages!.removeAt(deletedIndex);
  //                          if (_file!.pages!.length >= currentPage)
  //                            currentPage = _file!.pages!.length - 1;
  //                          if (_file!.pages!.isEmpty) {
  //                            _file!.pages!.add(XppPage.empty(
  //                                background: Theme.of(context).cardColor));
  //                            currentPage = 0;
  //
  //                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //                                content: Text(S
  //                                    .of(context)
  //                                    .thereWereNoMorePagesWeAddedOne)));
  //                          }
  //                        }),
  //                    onPageMove: (initialIndex, movedTo) => setState(() {
  //                          final page = _file!.pages![initialIndex];
  //                          _file!.pages!.removeAt(initialIndex);
  //                          _file!.pages!.insert(movedTo - 1, page);
  //                        }),
  //                    currentPage: currentPage),
  //                  ),
  //                  FloatingActionButton(
  //                    heroTag: 'AddXppPage',
  //                    onPressed: () => setState(() {
  //                      currentPage++;
  //                      _file!.pages!.add(XppPage.empty(background: Theme.of(context).cardColor));
  //                      setState(() {});
  //  //
  //  //                    _pageStackKey.currentState!
  //  //                        .setPageData(_file!.pages![currentPage]);
  //                    }),
  //                    child: Icon(Icons.add),
  //                    tooltip: S.of(context).addPage,
  //                  )
  //              ],
  //            )),
  //      ),
        floatingActionButtonLocation: kIsWeb
            ? FloatingActionButtonLocation.centerFloat
            : FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children:[
            FloatingActionButton(
              heroTag: 'formatPaint',
              onPressed: () {
                showModalBottomSheet(
                    elevation: 16,
                    backgroundColor: Theme.of(context).backgroundColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16),
                          topRight: Radius.circular(16)),
                    ),
                    context: context,
                    builder: (context) => ToolBoxBottomSheet(
                          onBackgroundChange: (newBackground) {
                            newBackground.size = _file!.pages![currentPage].pageSize;
                            setState(() => _file!.pages![currentPage].background =
                                newBackground);
                          },
                        ));
              },
              tooltip: S.of(context).tools,
              child: Icon(Icons.format_paint),
            ), // This trailing comma makes auto-formatting nicer for build methods.
            FloatingActionButton(
              heroTag: 'AddXppPage',
              onPressed: () => setState(() {
                //currentPage++;
                _file!.pages!.add(XppPage.empty(background: Theme.of(context).cardColor));
                setState(() {});
    //
    //                    _pageStackKey.currentState!
    //                        .setPageData(_file!.pages![currentPage]);
              }),
              child: Icon(Icons.add),
              tooltip: S.of(context).addPage,
            ),
            FloatingActionButton(
              heroTag: 'RemoveXppPage',
              onPressed: () => setState(() {
                int lastPage = _file!.pages!.length-1;
  //              print(_file!.pages![lastPage].background);
  //              print((_file!.pages![lastPage].background as XppBackgroundPdf).filename);
                if (!(_file!.pages![lastPage].background is XppBackgroundPdf)) {
                //if (_file!.pages![lastPage].background!.page != null).cardColor) {
                  _file!.pages!.removeAt(lastPage);
                  currentPage=0;
                  setState(() {});
                }
    //
    //                    _pageStackKey.currentState!
    //                        .setPageData(_file!.pages![currentPage]);
              }),
              child: Icon(Icons.remove),
              tooltip: S.of(context).deletePage,
            ),
          ]
        ),
      ),
    );
  }

  void _setMetadata() {
    _file = widget.file;
		this._setScale((_file!.pages!.length.toDouble()), animate: false);
    //if (widget.filePath != null) filePath = widget.filePath;
  }

  Future<void> _showTitleDialog() async {
    await showDialog(
        context: context,
        builder: (context) {
          TextEditingController titleController =
              TextEditingController(text: _file!.title);
          return AlertDialog(
            title: Text(S.of(context).setDocumentTitle),
            content: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: TextField(
                  autofocus: true,
                  controller: titleController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: S.of(context).newTitle)),
            ),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).cancel),
              ),
              TextButton(
                onPressed: () {
                  setState(() {
                    _file!.title = titleController.text;
                  });
                  Navigator.of(context).pop();
                },
                child: Text(S.of(context).apply),
              ),
            ],
          );
        });
  }

  void setDefaultDeviceIfNotSet({PointerDeviceKind? kind}) {
    if (!_toolData.keys.contains(kind)) {
      EditingTool tool;
      switch (kind) {
        case PointerDeviceKind.touch:
          tool = EditingTool.MOVE;
          break;
        case PointerDeviceKind.invertedStylus:
          tool = EditingTool.ERASER;
          break;
        case PointerDeviceKind.stylus:
          tool = EditingTool.STYLUS;
          break;
        case PointerDeviceKind.mouse:
          tool = EditingTool.SELECT;
          break;
        default:
          tool = EditingTool.MOVE;
          break;
      }
      _toolData[kind] = tool;
    }
  }

  void _setZoomableState() {
    final zoomEnabled = _toolData[_currentDevice] == null ||
        _toolData[_currentDevice] == EditingTool.MOVE;
    _zoomableKey.currentState!
        .setState(() => _zoomableKey.currentState!.enabled = zoomEnabled);
    //singleChildScrollerState.currentState!.setState(() => singleChildScrollerState.currentState!.physics = zoomEnabled ? null : NeverScrollableScrollPhysics);
    //setState(() {});
    for (int i = 0; i<_file!.pages!.length; i++) {
      if (pointerListenerKeys.containsKey(i) && pointerListenerKeys[i] != null && pointerListenerKeys[i]!.currentState != null) {
        pointerListenerKeys[i]!.currentState!.setState(() {
          pointerListenerKeys[i]!.currentState!.drawingEnabled = !zoomEnabled;
        });
      }
    }
//    _pointerListenerKey.currentState!.setState(() {
//      _pointerListenerKey.currentState!.drawingEnabled = !zoomEnabled;
//    });
  }

  void _setScale(double newZoom, {animate = true}) {
    newZoom = max(.1, min(5, newZoom));
    if (newZoom != pageScale) {
      // final translation =
      //     _zoomController.value.getTranslation() * newZoom / pageScale;
      pageScale = newZoom;
      if (animate) {
        _animateTransformation(_zoomController.value.clone()
          ..setDiagonal(Vector4(newZoom, newZoom, 1, 1)));
        // ..setTranslation(translation));
      } else {
        _zoomController.value.setDiagonal(Vector4(newZoom, newZoom, 1, 1));
        // _zoomController.value.setTranslation(translation);
      }
      //setState(() {});
    }
  }

  void shareScreenshot() async {
    Uint8List imageBytes =
        await pageListViewKey.currentState!.getPng(currentPage);
    String fileName = await (FilePickerCross(imageBytes,
            fileExtension: '.png',
            path: '/export/' +
                (_file?.title ?? S.of(context).newFile) +
                ' ${currentPage + 1}' +
                '.png')
        .exportToStorage() as FutureOr<String>);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).successfullyShared + ' ' + fileName)));
  }

  void saveFile({bool export = false}) async {
    setState(() {
      savingFile = true;
    });
    //ScaffoldFeatureController snackBarController =
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(S.of(context).savingFile),
        //duration: Duration(days: 999),
      ),
    );
    //try {
    if (_file!.title == null) await _showTitleDialog();
    String path = _file!.title! + '.xopp';
    print("saving to: "+path);
//    _file!.previewImage = kIsWeb
//        ? kTransparentImage
//        : await pageListViewKey.currentState!.getPng(0);
    _file!.previewImage = kTransparentImage;
    FilePickerCross file = _file!.toFilePickerCross(filePath: path);
    if (export)
      file.exportToStorage();
    else
      file.saveToPath(path: path);

    /// starting async task to save recent files list
    SharedPreferences.getInstance().then((prefs) {
      String jsonData = prefs.getString(PreferencesKeys.kRecentFiles) ?? '[]';
      Set files = (jsonDecode(jsonData) as Iterable).toSet();
      files.removeWhere((element) => element['path'] == path);
      files.add({
        //'preview': base64Encode(_file!.previewImage!),
        'name': _file!.title,
        'path': path
      });
      jsonData = jsonEncode(files.toList());
      prefs.setString(PreferencesKeys.kRecentFiles, jsonData);
    });
    //snackBarController.close();
    setState(() {
      savingFile = false;
    });
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(S.of(context).successfullySaved),
      ),
    );
    /*} catch (e) {
      snackBarController.close();
      setState(() {
        savingFile = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content:
              Text(S.of(context).unfortunatelyThereWasAnErrorSavingThisFile),
        ),
      );
    }*/
  }

  void _onAnimationReset() {
    _zoomController.value = _animationReset!.value;
    if (!_controllerReset.isAnimating) {
      _animationReset?.removeListener(_onAnimationReset);
      _animationReset = null;
      _controllerReset.reset();
    }
  }

  void _animateTransformation(Matrix4 animateTo) {
    _controllerReset.reset();
    _animationReset = Matrix4Tween(
      begin: _zoomController.value,
      end: animateTo,
    ).animate(_controllerReset);
    _animationReset!.addListener(_onAnimationReset);
    _controllerReset.forward();
  }

  void _onInteractionStart(ScaleStartDetails details) {
    // If the user tries to cause a transformation while the reset animation is
    // running, cancel the reset animation.
    if (_controllerReset.status == AnimationStatus.forward) {
      _controllerReset.stop();
      _animationReset?.removeListener(_onAnimationReset);
      _animationReset = null;
      // assign animateTo value to skip to end
      // _zoomController.value = _animateTo;
    }
  }

  @override
  void dispose() {
    _controllerReset.dispose();
    super.dispose();
  }

  Color getColor(){
    return toolColor;
  }

  double getWidth(){
    // average pressure is 0.5, so divide by 2
    // see L266
    return toolWidth / 2;
  }

  void rememberToolSettings(){
    SharedPreferences.getInstance().then((prefs) {
      prefs.setInt(PreferencesKeys.kToolColor, toolColor.value);
      prefs.setDouble(PreferencesKeys.kToolWidth, toolWidth);
    });
  }

  void loadToolSettings(){
    SharedPreferences.getInstance().then((prefs) {
      if (prefs.getInt(PreferencesKeys.kToolColor) != null)
        toolColor = Color(prefs.getInt(PreferencesKeys.kToolColor)!);
      if (prefs.getDouble(PreferencesKeys.kToolWidth) != null)
        toolWidth = prefs.getDouble(PreferencesKeys.kToolWidth)!;
    });
  }
}
