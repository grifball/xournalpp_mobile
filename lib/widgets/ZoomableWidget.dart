import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' show Quad;

class ZoomableWidget extends StatefulWidget {
  //final Widget? child;
  @required
  final Widget Function(BuildContext, Quad)? builder;
  @required
  final TransformationController? controller;
  @required
  final GestureScaleUpdateCallback? onInteractionUpdate;
  final GestureScaleStartCallback? onInteractionStart;

  const ZoomableWidget(
      {Key? key,
      this.builder,
      this.controller,
      this.onInteractionUpdate,
      this.onInteractionStart})
      : super(key: key);

  @override
  ZoomableWidgetState createState() => ZoomableWidgetState();
}

class ZoomableWidgetState extends State<ZoomableWidget> {
  bool enabled = false;

  @override
  Widget build(BuildContext context) {
    return InteractiveViewer.builder(
      //boundaryMargin: const EdgeInsets.all(16),
      boundaryMargin:EdgeInsets.all(double.infinity),
      onInteractionStart: widget.onInteractionStart,
      onInteractionUpdate: widget.onInteractionUpdate,
      panEnabled: enabled,
      scaleEnabled: true,
      transformationController: widget.controller,
      minScale: 0.1,
      maxScale: 5,
      //child: widget.child!,
      builder: widget.builder!,
    );
  }
}
