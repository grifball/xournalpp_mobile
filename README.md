# <img src="assets/xournalpp-adaptive.png" width="64" style="height: auto;"/> Xournal++ Mobile

# Changes with this branch (```https://gitlab.com/grifball/xournalpp_mobile```)

## Note for the user:
Loading xopps with pdf backgrounds automatically wasn't working for me, so I added a button: "Import xopp and pdf". This will sequentially ask the user to first select an xopp page in the file browser, and then select a pdf background in the file browser. This process isn't desribed to the user in the UI at all, so communicating that is a big and easy TODO to make.

This has only been tested on Android on a microsoft surface duo. I may have broken things for ios/web/desktop.

While, through testing, it seems that this code is stable, I don't consider this code practical out of the box. You really have to know the limitations of this branch before you can use it. Also be tolerant of minor bugs like sometimes when you back out of edit mode, the app just becomes a black screen and you have to reopen the app. I don't consider this a high-priority issue since you don't lose any data and it's quick to relaunch the app.

## Biggest changes:
Pages now scroll vertically in the main editing window:
This was a huge change, using an InteractiveViewer with rows/columns and I got the code from looking at this video: ```https://www.youtube.com/watch?v=qax_nOpgz7E``` (TODO: make sure copying code from a youtube video doesn't violate any licenses or something?),
There might be people who prefer the old style of having a single page displayed, but for me, this wasn't an option because my surface duo has a big line running through the middle of it (and so if only a single page is displayed, the line will always block the middle of the page unless you zoom in, which is annoying to do everytime I want to read the middle of the page),
Also, allowing for scrolling all the pages in this way is nice because it means you can always move the page into a spot that is comfortable to use a pen on.

Pages also load WAY faster. This change is noted in ```https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/issues/59```

## Biggest TODO (ordered by importance/ease of implementation):

Have different configurable sizes for highlighter, pen, and eraser

An undo button:
This could be done in one of two ways:
Reversible actions - ```just call actions[actions.length-1].reverse(documentState)```,
Keeping track of state - ```documentState = previousStates.pop()```,
The second is much easier but uses more memory...

## Small improvements:
Fixed highlighter opacity (merge request with main branch ```https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/issues/47```)

Fixed opening xopps (had to remove the 'xopp' filter for this to work, which is a band-aid fix)

When you back out of canvas edit mode, the app now asks if you really meant to (in case you didn't save and accidentally triggered the back button while trying to zoom, which I did a lot while testing).

## small TODO:
Main drawer cant open anything (only the big button on the main "open" page)

Somehow the "SELECT" tool broke? (or did it actually ever work?) idk... but I want this back.

I'd like to have it so when you load an xopp and the original pdf path isnt there, it tries to load a pdf using the name of the xopp file in the same dir as the xopp file:
This conforms to how I use xournal and I'm writing this app (mostly) for me.

Make use of the two buttons on my bamboo pen (currently one doesn't do anything and the other is eraser. the eraser button is nice, but it would be nice to assign the other button to highlighter and have "no-button-mode" continue to be the pen)

## Bad changes (broken stuff):
In CanvasPage: bottom page horizontal scroll removed:
We dont need this anymore to change pages as the whole pdf scrolls vertically, so I removed it for performance/UI space.

Previews were removed:
Because the preview was being grabbed from the horizontal page scroller at the bottom of the canvas, I had to remove previews for now because I removed the bottom page scroller.
I don't care about previews (its always just a bunch of unreadable text for me) so this is very low priority to reintroduce.

(noted earlier) Sometimes when you back out of edit mode, the app becomes a black screen and you have to relaunch it.

After saving a file for the first time, it doesn't appear in the recent files list. Once you relaunch the app, it will appear.

You can't delete pdf pages and added pages are only appended to the end of the file.

## Notes on building (Android)

A lot of these tips might be obvious to any android/flutter developer, but I'm always switching dev tools/platforms, so I thought it might be nice to record my notes.

I had a lot of trouble getting this to build, but I managed to get it to work nicely. I think I have a version mismatch between flutter and dart? (I got a weird error and an SO post said it was because of a version mismatch).

I had to download an old version of flutter to get this to compile and ended up running this command:

```~/checkout/xournal_mobile$ ANDROID_HOME=~/Android/ ANDROID_SDK_ROOT=~/Android/sdk ~/Downloads/flutter/bin/flutter run --no-sound-null-safety -d <device id>```

The ```run``` command is great, really an awesome feature of flutter. After building and installing the apk with ```run```, the command will output log messages and listen for terminal input. If you press 'r' after running this on a device, it'll quick-compile and hot reload the code, which is often enough to test changes (it'll update the code in the same navigation window, so like things like changing widget sizes will automatically update). This function is also great for quickly finding compilation errors. If this doesn't work for testing some change, you can type 'R' to reset the app with the code. One note with the 'R' command, if you kill the app and relaunch it on the device, it'll run with old code and you have to hit 'R' again after relaunch to get it to run the new code. I think this quirk might've lead to some of my messy code as I thought the new code was running and kept making changes going a little insane. Only rarely do I have to kill the ```run``` command for changes and sometimes it'll crash.

```~/checkout/xournal_mobile$ ANDROID_HOME=~/Android/ ANDROID_SDK_ROOT=~/Android/sdk ~/Downloads/flutter/bin/flutter build apk --no-sound-null-safety```

Where you can grab the device id from:

```sudo adb devices```

If there's a permission error with ```run``` make sure you run ```adb``` with ```sudo```:

```adb kill-server```

```sudo adb devices```

Also, I had trouble generating a key for signing, it seems that you have to do:

```keytool -genkey -v -keystore ./test-keystore.jks -storetype JKS -keyalg RSA -keysize 4096 -validity 10000 -alias key```

The ```-storetype JKS``` is really important. Then you have to add this file?:

		~/checkout/xournalpp_mobile$ cat android/key.properties
		storePassword=123456
		keyPassword=123456
		keyAlias=key
		storeFile=/home/<username>/checkout/xm2/xournalpp_mobile/test-keystore.jks

I also put this in ```./key.properties``` in the main directory, but I think you have to have it in android. It's loaded in ```android/app/build.gradle``` with the lines:

    signingConfigs {
        release {
            keyAlias keystoreProperties['keyAlias']
            keyPassword keystoreProperties['keyPassword']
            storeFile keystoreProperties['storeFile'] ? file(keystoreProperties['storeFile']) : null
            storePassword keystoreProperties['storePassword']
        }
    }

You don't need to change these lines, just add the ```key.properties``` file and run ```keytool``` in the main directory.

I used an old version of flutter:

		$ ANDROID_HOME=~/Android/ ANDROID_SDK_ROOT=~/Android/sdk ~/Downloads/flutter/bin/flutter --version
		...
		Flutter 3.0.0 • channel stable • https://github.com/flutter/flutter.git
		Framework • revision ee4e09cce0 (1 year, 6 months ago) • 2022-05-09 16:45:18 -0700
		Engine • revision d1b9a6938a
		Tools • Dart 2.17.0 • DevTools 2.12.2

I believe you can get this version of flutter from ```https://docs.flutter.dev/release/archive?tab=linux```.

I also did a lot of shenanigans to get Android to work, like:

```~/Downloads/cmdline-tools$ ./bin/sdkmanager --install "cmdline-tools;latest" --sdk_root=/home/<username>/Android```

Not sure where I downloaded these tools from, probably here: ```https://developer.android.com/studio#command-line-tools-only```.

Accepting the licenses also gave me some trouble. I think I wasn't using ```ANDROID_HOME=~/Android/ ANDROID_SDK_ROOT=~/Android/sdk``` for a while and using android through ```apt``` wasn't working for me.

```ANDROID_HOME=~/Android/ ANDROID_SDK_ROOT=~/Android/sdk ~/Downloads/flutter/bin/flutter doctor --android-licenses```

Also had to change my java version at some point? ```update-alternatives --config java``` to: ```/usr/lib/jvm/java-11-openjdk-amd64/bin/java```.

That was before I found connectety's branch though, so maybe none of that was necessary: ```https://gitlab.com/connectety/xournalpp_mobile/-/tree/master```.

## Future work philosophy:

I'm leaving a few bugs unfixed since I'm thinking of completely changing the way the app works. I'd like it to function more like the desktop app (Xournal++), where you load xopps from the filesystem and save them directly back to the file system, instead of caching them in app memory. This would cut down on duplicated code (the recent file list is kinda like a duplication of the file system) while essentially having all the same functionality.

I'd like to cut out many features and only keep the ones essential to note taking. Or at least produce a "lite" version with only the essentials. It's good too have other features, but I'd like to off-load other features to other apps. One example is "recording" in the main app (Xournal++). I think this feature allowed you to record your voice while stepping through xopp pages? I'm not sure, but compiling the app with PortAudio always gave me trouble, and this feature can be provided by Android's screen recorder function, and similar on desktop so it seems like a waste to maintain and bloat the code with this. A more drastic example is pdf manipulation. Perhaps, instead of allowing the user to add/delete pdf pages in the app, maybe require the user to switch to another app to manipulate and save the pdf then reimport the pdf as a background in the xournal app. I'm not sure if that's a good change, since it's very convenient to manipulate the pdf pages in the app (like Xournal++ does) but that's just an example of the direction I'd like to take the app.

Note that I'm also willing to just make helpful changes to the main branch and my design philosophy doesn't have to interfere with that. I've only been working on this for a few days though, so I might lose interest. In any case, this (messy) code might be helpful to someone else who wants to pick up development of this app.

***Warning:*** *Xournal++ Mobile is currently in early development and **not** yet stable. Use with caution!*

[![Current version](https://img.shields.io/badge/dynamic/yaml?label=Current%20version&query=version&url=https%3A%2F%2Fgitlab.com%2FTheOneWithTheBraid%2Fxournalpp_mobile%2Fraw%2Fmaster%2Fpubspec.yaml%3Finline%3Dfalse&style=for-the-badge&logo=flutter&logoColor=white)](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/tags) [![Bitrise build](https://img.shields.io/bitrise/dd58f8fe5b4bf6c0?style=for-the-badge&token=Ihrbr8U0mqFlVBOocwtnQA&logo=bitrise&logoColor=white)](https://app.bitrise.io/app/dd58f8fe5b4bf6c0) [![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/TheOneWithTheBraid/xournalpp_mobile/master?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/pipelines) [![Google Play](https://img.shields.io/endpoint?color=689f38&url=https%3A%2F%2Fplayshields.herokuapp.com%2Fplay%3Fi%3Donline.xournal.mobile%26l%3DGoogle-Play%26m%3D%24version&style=for-the-badge&logo=google-play&logoColor=white)](https://play.google.com/store/apps/details?id=online.xournal.mobile) [![Snap Store](https://img.shields.io/badge/Get%20it%20from%20the-Snap%20Store-%230e8620?style=for-the-badge&logo=snapcraft&logoColor=white)](https://snapcraft.io/xournalpp-mobile)

A port of the main features of Xournal++ to various Flutter platforms like Android, iOS and the Web.

![Feature banner](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/raw/master/assets/feature-banner.svg)

## Try it out

***Mission completed:** We can now render strokes, images and text and LaTeX!. We thereby support the full `.xopp` file format.* :tada:

- Web
  - [Open web app](https://xournal.online/)
  - [Access via TOR](http://xournaltdtf7ygqxg3qik4tdg476smkukogil74t6oxqiwdnumy53hqd.onion/)
- Android
  - [Download in Google Play](https://play.google.com/store/apps/details?id=online.xournal.mobile)
  - [Download APK](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/jobs/artifacts/master/browse?job=build%3Aapk)
- Windows
  - [Build for Windows](#desktop-support)
- Linux
  - [Download for Debian](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/jobs/artifacts/master/browse?job=build%3Adebian)
  - [Download for generic Linux](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/jobs/artifacts/master/download?job=build%3Alinux)
  - [Download from the Snap Store](https://snapcraft.io/xournalpp-mobile)

```shell
sudo snap install xournalpp-mobile
```

### Visible parts already working

- [x] Read the document title
- [x] Read and display the number of pages
- [x] Create thumbnails of the pages for the navigation bar
- [x] Smooth fade in after thumbnail rendering
- [x] Render images on the canvas
- [x] Render text on the canvas
- [x] Strokes
- [x] Highlighter
- [x] LaTeX
- [x] Recent files list
- [ ] Whiteout eraser
- [x] Saving
- [x] Basic editing
- [x] Basic PDF rendering

## Known issues

- **Immense memory consumption**: *If you open immense files, you get immense memory consumption. That's logic. Usually, Xournal++ Mobile takes twice the file size plus around 50MB for itself.*
- But **why** does it take twice the memory?: *No idea. ¯\\\_(ツ)_/¯*
- **The snap does not start on Linux when using wayland**: *Please set the environment variable `DISABLE_WAYLAND=1` before you start Xournal++ Mobile.*

## Getting started

### Prepare

> You would like to contribute? Please check out issues to solve [here](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile/-/issues) or get our `// TODO:`s [here](https://gitlab.com/search?search=TODO&project_id=20056916)!

*The **GitHub** repository is only a mirrored repository. Please only contribute to the [original repository on **GitLab**](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile).*

Get your information about the `.xopp` file format at http://www-math.mit.edu/~auroux/software/xournal/manual.html#file-format .

Install Flutter first. See [flutter.dev](https://flutter.dev/docs/get-started/install) for more details.

```shell
# Run Flutter doctor to check whether the installation was successful
flutter doctor
```

### Get the sources and run

Connect any Android or iOS device.

```shell
git clone https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile.git
cd xournalpp_mobile
flutter run --no-sound-null-safety
```

### Test for the web

If you want to test for the web, please run:

```shell
flutter channel beta
flutter upgrade
flutter config --enable-web
flutter run -d web --release --no-sound-null-safety # unfortunately, the debug flavour will result an empty screen
```

### Desktop support

Linux is perfectly supported by Xournal++ Mobile and you can get prebuilt binaries [above](#try-it-out) or install from [Snap Store](https://snapcraft.io/xournalpp-mobile).

Windows is supported and tested too, but there are unfortunately no prebuilt binaries available. Execute the following commands to build them yourself.

If you want to test for Linux, Windows or macOS, please run:

```shell
flutter channel master
flutter upgrade
flutter config --enable-linux-desktop # or --enable-macos-desktop or --enable-windows-desktop
flutter run -d linux --no-sound-null-safety # or macos or windows
```

## Colors and Typography

### Colors

Our primary color is the Material DeepPurple. I simply prefer a colorful application over an old-fashioned gray GTK+ application.

`#673ab7` / `rgb(103, 58, 183)` / `CMYK(44%, 68%, 0%, 28%)` / `hsl(261°, 51%, 48%)`

The accent color is Material Pink.

`#e91e63` / `rgb(233, 30, 99)` / `CMYK(0%, 87%, 58%, 9%)`/ `hsl(340°, 81%, 51%)`

The light color is White.

`#ffffff` / `rgb(255, 255, 255)` / `CMYK(0%, 0%, 0%, 0%)`/ `hsl(0°, 0%, 100%)`

The dark color is Material Blue Grey 900.

`#263238` / `rgb(38, 50, 56)` / `CMYK(32%, 11%, 0%, 78%)`/ `hsl(200°, 19%, 18%)`

### Fonts

- Display Text: Open Sans Extra Bold *(800)* `Apache 2.0`, *accent color* or *light color*
- Title and Heading: Open Sans Regular *(400)* `Apache 2.0`, *light color*
- Emphasis: Glacial Indifference Regular *(400)* `SIL Open Font License`, *light color*, *UPPERCASE*
- Body: Open Sans Light *(300)* `Apache 2.0`, *light color*

## Misc

*Like this project? [Buy me a Coffee](https://buymeacoff.ee/braid).*

This software is powered by the education software [TestApp](https://testapp.schule) — **Learning. Easily.**

[![TestApp banner](https://gitlab.com/testapp-system/testapp-flutter/-/raw/mobile/assets/Google%20Play%20EN.png)](https://testapp.schule)

## Legal notes

This project is licensed under the terms and conditions of the EUPL-1.2 found in [LICENSE](LICENSE).
